
def max(xs: List[Int]): Int ={
  if(xs.isEmpty) throw new java.util.NoSuchElementException("Empty list")
  if(xs.size == 1) xs.head
  else{
    val maxTail = max(xs.tail)
    if(maxTail > xs.head) maxTail else xs.head
  }
}
//Autre solution
def max(xs: List[Int]): Int = xs match{
  case Nil => throw new NoSuchElementException()
  case head:: Nil => head
  case  head::tail =>
    val maxTail = max (tail)
    if(maxTail>head) maxTail
    else head
}

/*Exercice 2*/
def sum(xs:List(Int)):Int = xs match {
  case Nil => 0
  case  n::rest => n + sum(rest)
}

import annotation.tailrec
/*Exercice 2*/
def sum(xs: List[Int]) : Int ={
  @tailrec
  def helper ( xs: List[Int], acc: Int): Int = xs match {
    case Nil => acc
    case n:: rest => helper(rest, n+acc)
  }
  helper(xs,0)
}

def pascal(colonne: Int, ligne: Int): Int ={
  
}

def trianglePascal(n: Int): Unit = {
  for(a <- 0 to n-1){
    for (b<-0 to a){
      print(pascal(b,a)+" ")
      println()
    }
  }
}

// Start writing your ScalaFiddle code here
type Set = Int => Boolean
def isPair (n: Int): Boolean = {
  if(n%2==0) true else false
}
def isOne(i:Int): Boolean = i==1
def contains(s: Set, elem: Int): Boolean = s(elem)
def singletonSet(elem: Int): Set =  newElem => newElem == elem
def union(s: Set, t: Set): Set = elem => contains(s,elem)||contains(t,elem)
def intersect(s: Set, t: Set): Set = elem => contains(s,elem)&&contains(t,elem)
def diff(s: Set, t: Set): Set =  elem => contains(s,elem) && !contains(t,elem)

