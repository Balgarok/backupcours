package programmtionFonctionnelleTd;

public class Exercice2Q1 {

	public static void main(String[] args) {
		System.out.println(nbMinuscules("test"));
	}
	
	public static int nbMinuscules(String s) {
		int nbPetits = 0;
		for (int i = 0 ; i<s.length(); i++) {
			if(s.substring(i, i+1).matches("[a-z]")) {
				nbPetits++;
			}
		}
		return nbPetits;
	}
}
