package programmtionFonctionnelleTd;

import java.util.Scanner;

public class itExercice1Q2 {

	public static void main(String[] variables) {
        Scanner clavier = new Scanner(System.in);
 
        int i, n;
 
        // Fournir les donn�es d'entr�e
        System.out.print("Saisir un nombre n : ");
        n = clavier.nextInt();
 
        System.out.println("les nombres naturels de " + n + " � 1");
 
        for (i = n; i >= 1; i--) {
            System.out.print(i + "\t");
        }
 
        // fermer les ressources
        clavier.close();
    }
}
