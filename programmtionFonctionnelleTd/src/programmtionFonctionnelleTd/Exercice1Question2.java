package programmtionFonctionnelleTd;

import java.util.Scanner;

public class Exercice1Question2 {

	public static void main(String[] args) {
		//start();
		fun(5);
	}

	// Correction
	static void fun(int n) {
		if(n<=0) {
			return;
		}else {
			System.out.println(n+"\t");
			fun(n-1);
			
		}
	}
	
	// fait en cours
	public static void start() {
		Scanner clavier = new Scanner(System.in);
		System.out.println("Saisir un nombre n : ");
		int n = clavier.nextInt();
		System.out.println("les nombres naturels de "+n+" � 1");
		System.out.println(affiche(n,0));
	}
	
	public static String affiche(int n, int i) {
		if(i==n-1) {
			return (n-i)+"\t";
		}
		else {
			return ""+(n-i)+"\t"+affiche(n,i+1);
		}
	}
}
