package programmtionFonctionnelleTd;

import java.util.Scanner;

public class Exercice2Q2 {

	public static void main(String[] args) {
		System.out.println(nbMinuscules("GG"));
	}

	
	
	public static int nbMinuscules(String s) {
		int index=0;
		int nbPetits = 0;
		nbPetits = recurMinus(s,index);
		return nbPetits;
	}
	
	public static int recurMinus(String s , int index){
		if(index==s.length()) {
			return 0;
		}else {
			if(s.substring(index, index+1).matches("[a-z]")) {
				return 1+recurMinus(s, index+1);
			}else {
				return 0+recurMinus(s, index+1);
			}
		}
	}
}
