package programmtionFonctionnelleTd;

import java.util.Scanner;

public class Exercice1Question4 {

	public static void main(String[] args) {
		start();
	}

	public static void start() {
		Scanner clavier = new Scanner(System.in);
		System.out.println("Saisir un nombre n : ");
		int n = clavier.nextInt();
		System.out.println("les nombres impairs de 1 � " + n + " sont : ");
		System.out.println(affiche(n,1));
	}
	
	public static String affiche(int n, int i) {
		if(i>=n) {
			if(i%2==1 && i==n) {
				return n+"\t";
			}
			else {
				return "";
			}
		}
		else {	
			return ""+i+"\t"+affiche(n,i+2);	
		}
	}
}
