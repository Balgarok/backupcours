package programmtionFonctionnelleTd;

import java.util.ArrayList;
import java.util.List;

public class Exercice3Q2 {

	public static void main(String[] args) {
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		list1.add("test");
		list1.add("ca");
		list2.add("et");
		list2.add("fonctionne");
		List<String> renvoi = concat(list1,list2);
		System.out.println(renvoi);
	}
	
	public static <A> List<A> concat(List<A> l1, List<A>l2) {
		if(l2.isEmpty()) {
			return l1;
		}else {
			l1.add(l2.remove(0));
			return concat(l1,l2);
		}
	}
}
