package programmtionFonctionnelleTd;

import java.util.Scanner;

public class Exercice1Question1 {
	
	public static void main(String[] args) {
		//start();
		fun(5);
	}

	// Correction
	static void fun(int n) {
		if(n<=0) {
			return;
		}else {
			fun(n-1);
			System.out.println(n+"\t");
		}
	}
	
	// fait en cours
	public static void start() {
		Scanner clavier = new Scanner(System.in);
		System.out.println("Saisir un nombre n : ");
		int n = clavier.nextInt();
		System.out.println("les nombres naturels de 1 � " + n);
		System.out.println(affiche(n,1));
	}
	
	public static String affiche(int n, int i) {
		if(i==n) {
			return n+"\t";
		}
		else {
			return ""+i+"\t"+affiche(n,i+1);
		}
	}
}
