package programmtionFonctionnelleTd;

import java.util.Scanner;

public class itExercice1Q4 {

	public static void main(String[] variables) {
        Scanner clavier = new Scanner(System.in);
 
        int i, n;
 
        // Fournir les donn�es d'entr�e
        System.out.print("Saisir un nombre n : ");
        n = clavier.nextInt();
 
        System.out.println("les nombres impairs de 1 � " + n + " sont : ");
 
        for (i = 1; i <= n; i++) {
            // V�rifier si i n'est pas pair !
            if (i % 2 != 0) {
                System.out.print(i + "\t");
            }
        }
 
        // fermer les ressources
        clavier.close();
    }
}
