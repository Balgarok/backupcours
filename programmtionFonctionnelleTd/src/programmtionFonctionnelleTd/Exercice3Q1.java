package programmtionFonctionnelleTd;

import java.util.ArrayList;
import java.util.List;

public class Exercice3Q1 {
	
	public static void main(String[] args) {
		List<String> list1 = new ArrayList<String>();
		List<String> list2 = new ArrayList<String>();
		list1.add("test");
		list1.add("ca");
		list2.add("et");
		list2.add("fonctionne");
		while(!list2.isEmpty()) {
			list1.add(list2.remove(0));
		}
		String phrase = list1.toString();
		System.out.println(phrase);
	}

}
